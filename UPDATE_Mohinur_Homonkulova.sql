UPDATE film
SET rental_duration = 3,
    rental_rate = 9.99
WHERE film_id = 1001;

UPDATE customer
SET first_name = 'Mohinur',
    last_name = 'Homonkulova',
    email = 'Mohinur_Homonkulova@student.itpu.uz',
    address_id = 6,
    create_date = current_date
WHERE customer_id = 1;