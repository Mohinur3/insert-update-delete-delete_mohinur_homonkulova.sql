INSERT INTO film (title, rental_rate, rental_duration, language_id)
VALUES ('Malifisenta', 4.99, 2, 1);

INSERT INTO actor (first_name, last_name)
VALUES ('Scarlett', 'Johansson'),
       ('Amr', 'Waked'),
       ('Morgan', 'Freeman');

INSERT INTO film_actor (actor_id, film_id)
VALUES (201, 1001),
       (202, 1001),
       (203, 1001);
       
INSERT INTO inventory (film_id, store_id)
VALUES (1001, 1);